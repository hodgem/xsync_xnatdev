package org.nrg.xsync.services.local;

/**
 * @author Mohana Ramaratnam
 *
 */
public interface MonthlySyncService {
	void syncMonthly();
}
