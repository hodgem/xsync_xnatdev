package org.nrg.xsync.remote.alias;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.springframework.stereotype.Repository;

/**
 * @author Mohana Ramaratnam
 *
 */
@Repository

public class RemoteAliasEntityRepository extends AbstractHibernateDAO<RemoteAliasEntity>{

}
