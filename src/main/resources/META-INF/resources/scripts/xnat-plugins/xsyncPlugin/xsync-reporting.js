
window.XNAT  = getObject(window.XNAT);
window.XSYNC = getObject(window.XSYNC);

// keep it private
(function(XNAT, XSYNC){

	var reporting = XSYNC.reporting = getObject(XSYNC.reporting);
	var projectContext = XNAT.data.context.project;
	var xhr = XNAT.xhr;

	function xsyncUrl(part){
		return XNAT.url.rootUrl('/xapi/xsync' + part||'');
	}

	function localDate(date){
		return date.toLocaleDateString()
	}

	function localTime(date){
		return date.toLocaleTimeString()
	}

	reporting.showHistoryTable = function() {

		// Displays overview of sync history in table format
		var xsyncHistory = XNAT.table({ className: 'xnat-table sortable' });
		xsyncHistory.tr();
		xsyncHistory
				.th({className:'sort', html: 'Date <i>&nbsp;</i>'})
				.th({className:'sort', html: 'Status <i>&nbsp;</i>'})
				.th('Subjects')
				.th('Subject Assessments')
				.th('Derived Assessments')
				.th('Project Resources')
				.th({className:'sort', html: 'Total <i>&nbsp;</i>'});

		var getSyncHistory = xhr.getJSON(xsyncUrl('/history/projects/' + projectContext));

		getSyncHistory.done(function(data) {

			var allHistory = data.map(function(item, i){

				var date = new Date(item.startDate);

				return [
					'<div class="mono">' +
						'<i class="hidden start-time">' + item.startDate + '</i>' +
						'<a class="show-details link" title="' + item.id + '" href="#!">'+ localDate(date) + '<br>' + localTime(date) +'</a>' +
					'</div>',

					item.syncStatus,

					'<div class="mono centered">' + item.totalSubjects + '</div>',

					'<div class="mono centered">' + item.totalExperiments + '</div>',

					'<div class="mono centered">' + item.totalAssessors + '</div>',

					'<div class="mono centered">' + item.totalResources + '</div>',

					'<div class="mono align-right">' +
						'<i class="hidden total-data-synced">' + item.totalDataSynced + '</i>' +
						sizeFormat(item.totalDataSynced, 2) +
					'</div>'

				];
			});

			xsyncHistory.rows(allHistory.reverse());

			if (data.length) {
				$("#xsync-history-header").show();
				$("#xsync-history-table").append(xsyncHistory.table);
			}
			else {
				$("#xsync-history-table").spawn('p', 'No sync history.')
			}

		});

		// delegate a single event handler for all rows
		$(xsyncHistory.table).on('click', 'a.show-details', function(e){
			e.preventDefault();
			reporting.showHistoryDetailsModal(xsyncUrl('/history/'+this.title))
		});

	};

	reporting.showHistoryDetailsModal = function(uri) {
		xhr.getJSON(uri).done( function(history) {
			// Create the modal
			var startDate = new Date(history.startDate);
			xmodal.open({
				title: 'Xsync History for '+ projectContext + ' on '+ localDate(startDate) + ' ' + localTime(startDate),
				width: '80%',
				maxWidth: 1000,
				height: '95%',
				overflow: 'auto',
				maximize: true,
				content: '<div id="xsync-details-modal"></div>',
				beforeShow: function(obj){
					var container = obj.$modal.find('#xsync-details-modal');
					spawnXsyncHistoryTabs(container, history);
				},
				buttons: {
					close: {
						label: 'Close'
					}
				}
			});
		});
	};

	function spawnXsyncHistoryTabs(container, history) {
		XNAT.tabs.container = container;
		XNAT.spawner.spawn({
			myTabs: {
				kind: 'tabs',
				contains: 'tabs',
				label: 'Xsync History Detail',
				layout: 'left',
				name: 'xsyncHistoryTabs',
				tabs: {
					overview: generateOverviewTab(history),
					subjects: generateHistoryTab('Subjects', history.subjectHistories),
					experiments: generateHistoryTab('Subject Assessments', history.experimentHistories),
					assessors: generateHistoryTab('Derived Assessments', history.assessorHistories),
					resources: generateHistoryTab('Project Resources', history.resourceHistories)
				}
			}
		}).render(container, 100)
	}

	function generateOverviewTab(history) {

		var startDate = new Date(history.startDate);
		var completeDate = new Date(history.completeDate);

		return {
			kind: 'tab',
			name: 'overviewTab',
			label: 'Overview',
			group: 'xsyncGroup',
			active: 'true',
			contents: {
				overview: {
					kind: 'panel',
					label: 'History Overview',
					footer: false,
					contents: {
						syncStatus: {
							kind: 'panel.element',
							label: 'Status',
							contents: history.syncStatus
						},
						started: {
							kind: 'panel.element',
							label: 'Started',
							contents: localDate(startDate)+ ' ' + localTime(startDate)
						},
						completed: {
							kind: 'panel.element',
							label: 'Completed',
							contents: localDate(completeDate)+ ' ' + localTime(completeDate)
						},
						destinationXnat: {
							kind: 'panel.element',
							label: 'Destination XNAT',
							contents: history.remoteHost
						},
						remoteProject: {
							kind: 'panel.element',
							label: 'Destination Project',
							contents: history.remoteProject
						},
						totalSubjects: {
							kind: 'panel.element',
							label: 'Total Subjects',
							contents: history.totalSubjects.toString()
						},
						totalExperiments: {
							kind: 'panel.element',
							label: 'Total Subject Assessments',
							contents: history.totalExperiments.toString()
						},
						totalAssessors: {
							kind: 'panel.element',
							label: 'Total Derived Assessments',
							contents: history.totalAssessors.toString()
						},
						totalResources: {
							kind: 'panel.element',
							label: 'Total Resources',
							contents: history.totalResources.toString()
						},
						totalDataSynced: {
							kind: 'panel.element',
							label: 'Total Data',
							contents: history.totalDataSynced
						},
						syncUser: {
							kind: 'panel.element',
							label: 'Sync User',
							contents: history.syncUser
						}
					}
				}
			}
		}
	}

// String tomfoolery to generate similarly formatted tabs
	function generateHistoryTab(tabType, data) {

		var panelContent;

		if (data.length) {
			panelContent = XNAT.table.dataTable(data, {
				id: tabType.toLowerCase() + '-table',
				items: {
					localLabel: "Label",
					syncStatus: "Status",
					syncMessage: "Message"
				}
			}).get()
		}
		else {
			panelContent = '<i class="pad20h">Nothing synced.</i>';
		}

		var tabContent = {
			kind: 'panel',
			label: tabType + ' Sync Details',
			footer: false,
			contents: {
				history: {
					tag: 'div.pad20v',
					content: panelContent
				}
			}
		};

		return {
			kind: 'tab',
			name: tabType.toLowerCase() + ' Tab',
			label: tabType,
			contents: {
				tabTable: tabContent
			}
		}

	}

})(window.XNAT, window.XSYNC);
