<%@ page session="true" contentType="text/html" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="pg" tagdir="/WEB-INF/tags/page" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<pg:wrapper>
    <pg:xnat title="Info.">

        <jsp:include page="content.jsp"/>

    </pg:xnat>
</pg:wrapper>
